#include<iostream>
#include<iomanip>
#include<ctime>
#include<cstdlib>

using namespace std;

void Grid(int [][10]);		// function prototype of function display forest
int Frodo_Move(int &,int ,int [][10],int &,int &,int &,int &);		// function prototype of function Frodo move
void Darkriders_Move(int &,int [][10],int &,int &,int &);		// function prototype of function Darkriders move
void Dark_Riders(int ,int [][10],int ,int &,int &);		// function prototype of function to spawn Darkriders
void Win(int [][10],int &);		// function prototype of function win condition
void Kill_Frodo(int [][10],int &,int &,int &);		// function prototype of function kill Frodo

int main(){
	srand(time(0));
	int way[10][10],		// array to store Frodo , Darkriders , exit
	    row_D,column_D,		// row and column of Darkriders
		row=0,column=0,		// row and column of Frodo
		check,player=1,		// value to check when win and value of Frodo
		move,darkriders=3,	// value to store way to move and number of Darkriders
		Darkriders=2,move_2,		// value of Darkriders and value to store way to move of ring
		move_D=0,level=1;	// value to store way to move of Darkriders and value to start level
		
		
	while(level<=10){		// loop of level
		for(int j=0;j<10;j++){
				for(int k=0;k<10;k++){
					way[j][k]=0;
				}
			}
			way[0][0]=1;
			way[9][9]=3;
			row=0;
			column=0;
			
			Dark_Riders(Darkriders,way,darkriders,row_D,column_D);		// spawn Darkriders
			darkriders=darkriders+3;		// increase Darkriders per level
		do{	
				system("cls");
				check=9;
				cout<<"Level: "<<level<<endl;
				cout<<"1=diagonally down left,2=Up,3=diagonally down right,"<<endl<<"4=Left,6=Right,7=diagonally up left,"<<endl<<"8=Down,9=diagonally up right\n";
				cout<<"Press 5 to use ring\n";
				Grid(way);
				Win(way,check);		// check win
			if(check!=50){
				cout<<"Which way do you want to move ?: ";
				cin>>move;
				Frodo_Move(player,move,way,row,column,move_2,check);		// move Frodo
				Darkriders_Move(move_D,way,row_D,column_D,check);		// move Darkriders randomly
				cout<<endl;	
			}
			if(check==50){		// win condition		
				cout<<"You win!\n";
			}
			if(check==20){		// dead condition
				level=level+50;
			}
		}while(check==9);
		level++;		// increase level
	}
	
	return 0;
}
void Win(int way[][10],int &check){		// function win condition
	if(way[9][9]==1){
				check=50;
	}
}
int Frodo_Move(int &player,int move,int way[][10],int &row,int &column,int &move_2,int &check){		// function Frodo move
	if(player==1)
		{
			if(move==7){		// move diagonally up left
				if(way[row-1][column-1]==2){
						way[row-1][column-1]=2;
						way[row][column]=0;
						cout<<"\nYou dead!\n";
						check=20;
					}
				if(row-1>=0&&column-1>=0){
					way[row-1][column-1]=player;
					way[row][column]=0;
					row=row-1;
					column=column-1;
					Kill_Frodo(way,check,row,column);
				}else{		// Frodo will die if move out of forest
					cout<<"\nYou dead!\n";
					check=20;
				}	
			}
			if(move==8){		// move up
				if(way[row-1][column]==2){
						way[row-1][column]=2;
						way[row][column]=0;
						cout<<"\nYou dead!\n";
						check=20;
					}
				if(row-1>=0){
					way[row-1][column]=player;
					way[row][column]=0;
					row=row-1;
					column=column;
					Kill_Frodo(way,check,row,column);
				}else{		// Frodo will die if move out of forest
					cout<<"\nYou dead!\n";
					check=20;
				}	
			}	
			if(move==9){		// move diagonally up right
				if(way[row-1][column+1]==2){
						way[row-1][column+1]=2;
						way[row][column]=0;
						cout<<"\nYou dead!\n";
						check=20;
					}
				if(row-1>=0&&column+1<=9){
					way[row-1][column+1]=player;
					way[row][column]=0;
					row=row-1;
					column=column+1;
					Kill_Frodo(way,check,row,column);
				}else{		// Frodo will die if move out of forest
					cout<<"\nYou dead!\n";
					check=20;
				}	
			}
			if(move==4){		// move left
				if(way[row][column-1]==2){
					way[row][column-1]=2;
					way[row][column]=0;
					cout<<"\nYou dead!\n";
					check=20;
				}
				if(column-1>=0){
					way[row][column-1]=player;
					way[row][column]=0;
					row=row;
					column=column-1;
					Kill_Frodo(way,check,row,column);
				}else{		// Frodo will die if move out of forest
					cout<<"\nYou dead!\n";
					check=20;
				}	
			}
			if(move==6){		// move right
				if(way[row][column+1]==2){
					way[row][column+1]=2;
					way[row][column]=0;
					cout<<"\nYou dead!\n";
					check=20;
				}
				if(column+1<=9){
					way[row][column+1]=player;
					way[row][column]=0;
					row=row;
					column=column+1;
					Kill_Frodo(way,check,row,column);
				}else{		// Frodo will die if move out of forest
					cout<<"\nYou dead!\n";
					check=20;
				}	
			}
			if(move==1){		// move diagonally down left
				if(way[row+1][column-1]==2){
					way[row+1][column-1]=2;
					way[row][column]=0;
					cout<<"\nYou dead!\n";
					check=20;
				}
				if(row+1<=9&&column-1>=0){
					way[row+1][column-1]=player;
					way[row][column]=0;
					row=row+1;
					column=column-1;
					Kill_Frodo(way,check,row,column);
				}else{		// Frodo will die if move out of forest
					cout<<"\nYou dead!\n";
					check=20;
				}	
			}
			if(move==2){		// move down
				if(way[row+1][column]==2){
					way[row+1][column]=2;
					way[row][column]=0;
					cout<<"\nYou dead!\n";
					check=20;
				}
				if(row+1<=9){
					way[row+1][column]=player;
					way[row][column]=0;
					row=row+1;
					column=column;
					Kill_Frodo(way,check,row,column);
				}else{		// Frodo will die if move out of forest
					cout<<"\nYou dead!\n";
					check=20;
				}	
			}
			if(move==3){		// move diagonally down right
				if(way[row+1][column+1]==2){
					way[row+1][column+1]=2;
					way[row][column]=0;
					cout<<"\nYou dead!\n";
					check=20;
				}
				if(row+1<=9&&column+1<=9){
					way[row+1][column+1]=player;
					way[row][column]=0;
					row=row+1;
					column=column+1;
					Kill_Frodo(way,check,row,column);
				}else{		// Frodo will die if move out of forest
					cout<<"\nYou dead!\n";
					check=20;
				}	
			}
			if(move==5){		// use ring
				for(int i=0;i<4;i++){
					cout<<"You use ring and you can move 3 times but once per level\n";
					cout<<"choose the way: ";
					cin>>move_2;	
			if(move_2==7){
				if(way[row-1][column-1]==2){
						way[row-1][column-1]=2;
						way[row][column]=0;
						cout<<"\nYou dead!\n";
						i=4;
						check=20;
					}
				if(row-1>=0&&column-1>=0){
					way[row-1][column-1]=player;
					way[row][column]=0;
					row=row-1;
					column=column-1;
					Grid(way);
					if(way[9][9]==1){
						i=4;
						check=50;
					}
				}else{
					cout<<"\nYou dead!\n";
					i=4;
					check=20;
				}
			}
			if(move_2==8){
				if(way[row-1][column]==2){
						way[row-1][column]=2;
						way[row][column]=0;
						cout<<"\nYou dead!\n";
						i=4;
						check=20;
					}
				if(row-1>=0){
					way[row-1][column]=player;
					way[row][column]=0;
					row=row-1;
					column=column;
					Grid(way);
					if(way[9][9]==1){
						i=4;
						check=50;
					}
				}else{
					cout<<"\nYou dead!\n";
					check=20;
					i=4;
				}
			}
			if(move_2==9){
				if(way[row-1][column+1]==2){
						way[row-1][column+1]=2;
						way[row][column]=0;
						cout<<"\nYou dead!\n";
						i=4;
						check=20;
					}
				if(row-1>=0&&column+1<=9){
					way[row-1][column+1]=player;
					way[row][column]=0;
					row=row-1;
					column=column+1;
					Grid(way);
					if(way[9][9]==1){
						i=4;
						check=50;
					}
				}else{
					cout<<"\nYou dead!\n";
					check=20;
					i=4;
				}	
			}
			if(move_2==4){
				if(way[row][column-1]==2){
					way[row][column-1]=2;
					way[row][column]=0;
					cout<<"\nYou dead!\n";
					i=4;
					check=20;
				}
				if(column-1>=0){
					way[row][column-1]=player;
					way[row][column]=0;
					row=row;
					column=column-1;
					Grid(way);
					if(way[9][9]==1){
						i=4;
						check=50;
					}
				}else{
					cout<<"\nYou dead!\n";
					check=20;
					i=4;
				}
			}
			if(move_2==6){
				if(way[row][column+1]==2){
					way[row][column+1]=2;
					way[row][column]=0;
					cout<<"\nYou dead!\n";
					i=4;
					check=20;
				}
				if(column+1<=9){
					way[row][column+1]=player;
					way[row][column]=0;
					row=row;
					column=column+1;
					Grid(way);
					if(way[9][9]==1){
						i=4;
						check=50;
					}
				}else{
					cout<<"\nYou dead!\n";
					check=20;
					i=4;
				}
			}
			if(move_2==1){
				if(way[row+1][column-1]==2){
					way[row+1][column-1]=2;
					way[row][column]=0;
					cout<<"\nYou dead!\n";
					i=4;
					check=20;
				}
				if(row+1<=9&&column-1>=0){
					way[row+1][column-1]=player;
					way[row][column]=0;
					row=row+1;
					column=column-1;
					Grid(way);
					if(way[9][9]==1){
						i=4;
						check=50;
					}
				}else{
					cout<<"\nYou dead!\n";
					check=20;
					i=4;
				}	
			}
			if(move_2==2){
				if(way[row+1][column]==2){
					way[row+1][column]=2;
					way[row][column]=0;
					cout<<"\nYou dead!\n";
					i=4;
					check=20;
				}
				if(row+1<=9){
					way[row+1][column]=player;
					way[row][column]=0;
					row=row+1;
					column=column;
					Grid(way);
					if(way[9][9]==1){
						i=4;
						check=50;
					}
				}else{
					cout<<"\nYou dead!\n";
					check=20;
					i=4;
				}	
			}
			if(move_2==3){
				if(way[row+1][column+1]==2){
					way[row+1][column+1]=2;
					way[row][column]=0;
					cout<<"\nYou dead!\n";
					i=4;
					check=20;
				}
				if(row+1<=9&&column+1<=9){
					way[row+1][column+1]=player;
					way[row][column]=0;
					row=row+1;
					column=column+1;
					Grid(way);
					if(way[9][9]==1){
						i=4;
						check=50;
					}
				}else{
					cout<<"\nYou dead!\n";
					check=20;
					i=4;
				}
				}
			}
		}
	}
}
void Darkriders_Move(int &move_D,int way[][10],int &row_D,int &column_D,int &check){	// function Darkriders move
	int check_2=0;		// value to make Darkriders don't out of forest
	for(int row_D=0;row_D<10;row_D++){
		for(int column_D=0;column_D<10;column_D++){
			if(way[row_D][column_D]==2){
				move_D=rand()%10;
				while(move_D==0||move_D==5){		//use to make move!=5,0 
					move_D=rand()%10;
				}
			if(move_D==7){		// move diagonally up left
				if(row_D-1>=0&&column_D-1>=0){
					way[row_D-1][column_D-1]=2;
					way[row_D][column_D]=0;
					row_D=row_D-1;
					column_D=column_D-1;
					check_2=1;
					Kill_Frodo(way,check,row_D,column_D);		// call function kill Frodo
				}
			}
			if(move_D==8){		// move up
				if(row_D-1>=0){
					way[row_D-1][column_D]=2;
					way[row_D][column_D]=0;
					row_D=row_D-1;
					column_D=column_D;	
					check_2=1;
					Kill_Frodo(way,check,row_D,column_D);		// call function kill Frodo
				}
			}
			if(move_D==9){		// move diagonally up right
				if(row_D-1>=0&&column_D+1<=9){
					way[row_D-1][column_D+1]=2;
					way[row_D][column_D]=0;
					row_D=row_D-1;
					column_D=column_D+1;
					check_2=1;
					Kill_Frodo(way,check,row_D,column_D);		// call function kill Frodo
				}
			}
			if(move_D==4){		// move left
				if(column_D-1>=0){
					way[row_D][column_D-1]=2;
					way[row_D][column_D]=0;
					row_D=row_D;
					column_D=column_D-1;
					check_2=1;
					Kill_Frodo(way,check,row_D,column_D);		// call function kill Frodo
				}
			}
			if(move_D==6){		// move right
				if(column_D+1<=9){
					way[row_D][column_D+1]=2;
					way[row_D][column_D]=0;
					row_D=row_D;
					column_D=column_D+1;
					check_2=1;
					Kill_Frodo(way,check,row_D,column_D);		// call function kill Frodo
				}
			}
			if(move_D==1){		// move diagonally down left
				if(row_D+1<=9&&column_D-1>=0){
					way[row_D+1][column_D-1]=2;
					way[row_D][column_D]=0;
					row_D=row_D+1;
					column_D=column_D-1;
					check_2=1;
					Kill_Frodo(way,check,row_D,column_D);		// call function kill Frodo
				}
			}
			if(move_D==2){		// move down
				if(row_D+1<=9){
					way[row_D+1][column_D]=2;
					way[row_D][column_D]=0;
					row_D=row_D+1;
					column_D=column_D;
					check_2=1;
					Kill_Frodo(way,check,row_D,column_D);		// call function kill Frodo
				}
			}
			if(move_D==3){		// move diagonally down right
				if(row_D+1<=9&&column_D+1<=9){
					way[row_D+1][column_D+1]=2;
					way[row_D][column_D]=0;
					row_D=row_D+1;
					column_D=column_D+1;	
					check_2=1;
					Kill_Frodo(way,check,row_D,column_D);		// call function kill Frodo
				}
			}
			if(check==0){		// if Darkriders out of forest will random again
				Darkriders_Move(move_D,way,row_D,column_D,check);
			}
			}
		}
	}
}
void Kill_Frodo(int way[][10],int &check,int &row_D,int &column_D){		// function kill Frodo
				if(way[row_D+1][column_D+1]==1&&row_D<=9&&column_D<=9){		// check diagonally down right case			
					way[row_D+1][column_D+1]==2;
					way[row_D][column_D]=0;
					Grid(way);
					cout<<"You dead!";
					check=20;
				}else if(way[row_D+1][column_D]==1&&row_D+1<=9){		// check down case
					way[row_D+1][column_D]=2;
					way[row_D][column_D]=0;
					Grid(way);
					cout<<"You dead!";
					check=20;
				}else if(way[row_D+1][column_D-1]==1&&row_D+1<=9&&column_D-1>=0){		// check diagonally down left case
					way[row_D+1][column_D-1]=2;
					way[row_D][column_D]=0;
					Grid(way);
					cout<<"You dead!";
					check=20;
				}else if(way[row_D][column_D+1]==1&&column_D+1<=9){		// check right case
					way[row_D][column_D+1]=2;
					way[row_D][column_D]=0;
					Grid(way);
					cout<<"You dead!";
					check=20;
				}else if(way[row_D][column_D-1]==1&&column_D-1>=0){		// check left case
					way[row_D][column_D-1]=2;
					way[row_D][column_D]=0;
					Grid(way);
					cout<<"You dead!";
					check=20;
				}else if(way[row_D-1][column_D+1]==1&&row_D-1>=0&&column_D+1<=9){		// check diagonally up right case
					way[row_D-1][column_D+1]=2;
					way[row_D][column_D]=0;
					Grid(way);
					cout<<"You dead!";
					check=20;
				}else if(way[row_D-1][column_D]==1&&row_D-1>=0){		// check up case
					way[row_D-1][column_D]=2;
					way[row_D][column_D]=0;
					Grid(way);
					cout<<"You dead!";
					check=20;
				}else if(way[row_D-1][column_D-1]==1&&row_D-1>=0&&column_D>=0){		// check diagonally up left case
					way[row_D-1][column_D-1]=2;
					way[row_D][column_D]=0;
					Grid(way);
					cout<<"You dead!";
					check=20;
				}
}
void Dark_Riders(int Darkriders,int way[][10],int darkriders,int &row_D,int &column_D){		// function spawn Darkriders
	for(int i=0;i<darkriders;i++){
		if(way[0][0]==1){
			row_D=rand()%10;
			column_D=rand()%10;
			way[row_D][column_D]=Darkriders;
		}
		if(way[0][0]==2){		// if Darkriders spawn at Frodo start it will random again
			Dark_Riders(Darkriders,way,darkriders,row_D,column_D);
		}
		if(way[9][9]==2){		// / if Darkriders spawn at exit it will random again
			Dark_Riders(Darkriders,way,darkriders,row_D,column_D);
		}
	}
}
void Grid(int way[][10]){		// function display forest
		cout<<"------------------------------"<<endl;
	for(int i=0;i<10;i++){
		for(int j=0;j<10;j++){	
			cout<<"|";
			if(way[i][j]==0){
				cout<<" ";
			}
			if(way[i][j]==1){		
				cout<<"F";
			}
			if(way[i][j]==2){ 
	 			cout<<"R";
			}
			if(way[i][j]==3){ 
	 			cout<<"E";
			}
			cout<<"|";
	}
		cout<<endl;
		cout<<"------------------------------"<<endl;
			}
}
